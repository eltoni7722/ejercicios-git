<?php

use cursophp7\app\exception\QueryException;
use cursophp7\app\repository\AsociadoRepository;
use cursophp7\app\repository\ImagenGaleriaRepository;
use cursophp7\app\utils\Utils;
use cursophp7\core\App;


try{
    $imagenes=App::getRepository(ImagenGaleriaRepository::class)->findAll();


    $asociados=App::getRepository(AsociadoRepository::class)->findAll();

    //$asociados=Utils::extraerAsociados($asociados, 3);

    require __DIR__.'/../views/index.view.php';

}catch(QueryException $queryException){
    die($queryException->getMessage());
}



?>